package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDao;
import com.dto.Student;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String studentName = request.getParameter("studentName");
		String gender = request.getParameter("gender");
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
		
		Student std = new Student(0, studentName, gender, emailId, password);
		
		
		StudentDao stdDao = new StudentDao();
		int result = stdDao.studentRegister(std);
			

		out.println("<body bgcolor='lightyellow' text='green'>");
		out.println("<center>");
		
		if (result > 0) {			
			out.println("<h3> Employee Registered Successfully!! </h3>");			
			RequestDispatcher rd = request.getRequestDispatcher("Login.jsp");
			rd.include(request, response);			
		} else {			
			out.println("<h3 style='color:red;'> Failed to Register Employee!!! </h3>");
			RequestDispatcher rd = request.getRequestDispatcher("Register.jsp");
			rd.include(request, response);
		}
		out.println("</center></body>");

		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
