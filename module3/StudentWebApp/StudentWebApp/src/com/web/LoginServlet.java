package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.StudentDao;
import com.dto.Student;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");

		HttpSession session = request.getSession(true);
		session.setAttribute("emailId", emailId);

		out.println("<html>");
		out.println("<body bgcolor='lightyellow'><center>");

		if (emailId.equalsIgnoreCase("admin") && password.equals("admin")) {
			RequestDispatcher rd = request.getRequestDispatcher("AdminHomePage.jsp");
			rd.forward(request, response);

		} else {

			StudentDao stdDao = new StudentDao();
			Student std = stdDao.stdLogin(emailId, password);

			if (std != null) {

				// Storing student data under the Session for Profile
				session.setAttribute("std", std);

				RequestDispatcher rd = request.getRequestDispatcher("StudentHomePage.jsp");
				rd.forward(request, response);
			} else {
				out.println("<h1 style='color:red;'>Invalid Credentials</h1>");
				RequestDispatcher rd = request.getRequestDispatcher("Login.jsp");
				rd.include(request, response);
			}
		}

		out.println("</center></body></html>");

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
