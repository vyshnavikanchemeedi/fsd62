<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="com.dto.Student" %>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>GetEmployeeById</title>
</head>
<body>

	<jsp:include page="AdminHomePage.jsp" />
	
	<br/>
	<table align='center' border=2>

		<tr>
			<th>StudentId</th>
			<th>StudentName</th>
			<th>Gender</th>
			<th>EmailId</th>
		</tr>

		<tr>
			<td> ${ std.studentId   } </td>
			<td> ${ std.studentName } </td>
			<td> ${ std.gender  } </td>
			<td> ${ std.emailId } </td>
		</tr>

	</table>

</body>
</html>

