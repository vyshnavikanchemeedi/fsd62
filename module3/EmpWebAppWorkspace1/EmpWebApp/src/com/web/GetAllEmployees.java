package com.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDao;
import com.dto.Employee;

@WebServlet("/GetAllEmployees")
public class GetAllEmployees extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		EmployeeDao empDao = new EmployeeDao();
		List<Employee> empList = empDao.getAllEmployees();

		if (empList != null) {			
			request.setAttribute("empList", empList);			
			request.getRequestDispatcher("GetAllEmployees.jsp").forward(request, response);	
		} else {
			request.getRequestDispatcher("HRHomePage.jsp").include(request, response);
			out.println("<h3 style='color:red; align='center'>Unable to Fetch Employee Records!!</h3>");
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}
