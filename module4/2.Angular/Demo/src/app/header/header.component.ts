import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent implements OnInit {
  loginStatus: any;
  cartItems: any;

  constructor(private service: EmpService) {
    //Cart
    this.cartItems = service.cartItems;
  }

  ngOnInit() {
    this.service.getUserLoginStatus().subscribe((data: any) => { this.loginStatus = data; });
  }
}
