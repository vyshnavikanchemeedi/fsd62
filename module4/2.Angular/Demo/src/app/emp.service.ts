import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  isUserLoggedIn: boolean;
  loginStatus: any;

  //Cart
  cartItems: any;

  constructor(private http: HttpClient) {

    //Cart
    this.cartItems = [];

    this.isUserLoggedIn = false;
    this.loginStatus = new Subject();
  }

  //Cart
  addToCart(product: any) {
    this.cartItems.push(product);
  }

  //Cart
  getCartItems(): any {
    return this.cartItems;
  }
  
  setUserLoggedIn() {
    this.isUserLoggedIn = true;
    this.loginStatus.next(true);
  }
  setUserLoggedOut() {
    this.isUserLoggedIn = false;
    this.loginStatus.next(false);
  }
  getLoginStatus(): boolean {
    return this.isUserLoggedIn;
  }
  getUserLoginStatus(): any {
    return this.loginStatus.asObservable();
  }

  //Employee CRUD Operations
  //------------------------

  getAllEmployees() {
    return this.http.get('http://localhost:8085/getAllEmployees');
  }
  getAllDepartments() {
    return this.http.get('http://localhost:8085/getAllDepartments');
  }
  registerEmployee(employee: any) {
    return this.http.post('http://localhost:8085/addEmployee', employee);
  }
  employeeLogin(emailId: any, password: any) {
    return this.http.get('http://localhost:8085/empLogin/' + emailId + "/" + password).toPromise();
  }
  getEmployeeById(empId: any) {
    return this.http.get('http://localhost:8085/getEmployeeById/' + empId).toPromise();
  }
  deleteEmployeeById(empId: any) {
    return this.http.delete('http://localhost:8085/deleteEmployeeById/' + empId);
  }
  updateEmployee(employee: any) {
    return this.http.put('http://localhost:8085/updateEmployee', employee);
  }
  getAllProducts() {
    return this.http.get('http://localhost:8085/getAllProducts');
  }
  


  getAllCountries() {
    return this.http.get('https://restcountries.com/v3.1/all');
  }
}


