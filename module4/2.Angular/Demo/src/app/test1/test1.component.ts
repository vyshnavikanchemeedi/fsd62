import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test1',
  templateUrl: './test1.component.html',
  styleUrl: './test1.component.css'
})
export class Test1Component implements OnInit{
  // person = {
  //   id: 1,
  //   name: 'Vaishnavi',
  //   age: 20,
  //   hobbies: ['Reading', 'Traveling', 'Gaming'],
  //   address: {
  //     streetNo: '4-60',
  //     city: 'Hyderabad',
  //     state: 'Telangana'
  //   }

  person: any;

  constructor() {
    this.person = {
      id: 101,
      name: 'Vaishnavi',
      age: 22,
      hobbies: ['Running', 'Music', 'Movies', 'GYM', 'Reading'],
      address: {streetNo: 111, city: 'Hyd', state: 'Telangana'}
    };
  }
  ngOnInit(): void {
  }

  submit() {
    alert("Id:" + this.person.id + "\n" + "Name: " + this.person.name);
    console.log(this.person);
  }


}
