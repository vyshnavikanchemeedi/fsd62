import { Component } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  emp: any;

  constructor(private service: EmpService, private router: Router) {
  }

  async loginSubmit(loginForm: any) {
    console.log(loginForm);

    //Setting EmailId under LocalStorage
    localStorage.setItem('emailId', loginForm.emailId);

    if (loginForm.emailId == "HR" && loginForm.password == "HR") {

      this.service.setUserLoggedIn();
      this.router.navigate(['showemps']);

    } else {
      
      await this.service.employeeLogin(loginForm.emailId, loginForm.password).then((data: any) => {
        console.log(data);
        this.emp = data;
      });

      if (this.emp != null) {
        alert('Login Success');

        this.service.setUserLoggedIn();
        this.router.navigate(['products']);

      } else {
        alert('Invalid Credentials')
      }
     
    }
  }
}
