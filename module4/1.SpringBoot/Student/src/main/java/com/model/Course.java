package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Course {
	
	@Id
	private int courseId;
	private String courseName;
	private double courseFee;
	
	@OneToMany(mappedBy="Course")
	@JsonIgnore
	List<Student> stdList = new ArrayList<Student>();
	
	public Course() {
	}

	public Course(int courseId, String courseName, double courseFee) {
		this.courseId = courseId;
		this.courseName = courseName;
		this.courseFee = courseFee;
	}
	
	
	public List<Student> getStdList() {
		return stdList;
	}
	public void setStdList(List<Student> stdList) {
		this.stdList = stdList;
	}

	public int getCourseId() {
		return courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public double getcourseFee() {
		return courseFee;
	}
	public void setcourseFee(double courseFee) {
		this.courseFee = courseFee;
	}

	@Override
	public String toString() {
		return "Course [courseId=" + courseId + ", courseName=" + courseName + ", courseFee=" + courseFee + ", stdList="
				+ stdList + "]";
	}

	
}
