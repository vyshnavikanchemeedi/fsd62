package com.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

	@Autowired
	private JavaMailSender mailSender;



	public void sendEmail(String toEmail) {

		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom("vaishukanchemeedi33@gmail.com");
		message.setTo(toEmail);
		message.setText("This is a body of confirmation email");
		message.setSubject("Confirmation Email");

		mailSender.send(message);

		System.out.println("Mail Sent Successfully...");
	}
}
